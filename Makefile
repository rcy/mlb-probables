build:
	docker-compose build

up: build
	docker-compose up --remove-orphans

upd: build
	docker-compose up -d

down:
	docker-compose down

create-machine:
	docker-machine create --driver digitalocean --digitalocean-access-token=${TOKEN} probables

rm-machine:
	docker-machine rm probables

.PHONY: deploy
deploy:
	time sh ./deploy.sh
