require('dotenv').config()
const { sendSms } = require('./twilio')
const bodyParser = require('body-parser')
const moment = require('moment-timezone')
const express = require('express')
const app = express()
const db = require('./db')
const scrapeProbables = require('./scrape-probables')
const MessagingResponse = require('twilio').twiml.MessagingResponse;

console.log(process.env)

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', async (req, res) => {
  let text = '<table>'
  const starts = await db.allStarts({ side: 'home' })
  const arr = await starts.toArray();
  arr.forEach(start => {
    const cal = moment.tz(start.time, "America/Los_Angeles").calendar()
    text += `
<tr>
  <td>${cal}</td>
  <td>${start.team}</td>
  <td>${start.name || 'TBD'}</td>
</tr>
<tr>
  <td></td>
  <td>${start.vs.team}</td>
  <td>${start.vs.name || 'TBD'}</td>
</tr>
`
  })
  text += '</table>'
  res.send(text)
})

app.post('/incoming', async (req, res) => {
  try {
    const twiml = new MessagingResponse();
    const name = req.body.Body.trim()

    const start = await db.nextStart({ name })
    if (start) {
      const { name, time } = start
      twiml.message(`${name}'s next start is ${moment.tz(time, "America/Los_Angeles").calendar()}`);
    } else {
      twiml.message(`Couldn't find a start for ${name}!`);
    }

    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
  } catch(ex) {
    console.error(ex)
    res.writeHead(500)
    res.end("internal server error\n")
  }
})

app.post('/scrape', async (req, res) => {
  for (let n of [0,1,2,3,4,5,6,7]) {
    date = moment().add(n, 'day')
    await scrapeProbables(date).then(async (result) => {
      for (let p of result) {
        await db.addStart(p)
        await db.addPitcher(p)
        await db.addTeam(p)
      }
    })
    res.end("cool");
  }
})

const port = 4000
app.listen(port, () => console.log(`express listening on port ${port}`))
