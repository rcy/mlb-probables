const fs = require('fs');
const rp = require('request-promise')
const moment = require('moment')

const fetch = async (url) => {
  let html;
  const cacheDir = '/tmp/caching-fetch'
  const filename = `${cacheDir}/${encodeURIComponent(url)}`

  if (fs.existsSync(filename)) {
    if (moment().diff(fs.statSync(filename).ctime, 'minutes') < 5) {
      html = fs.readFileSync(filename, { encoding: 'utf8' })
    }
  }

  if (html) {
    console.log('HIT', url)
  } else {
    console.log('MISS', url)
    html = await rp(url)
    if (!fs.existsSync(cacheDir)){
      fs.mkdirSync(cacheDir)
    }
    fs.writeFileSync(filename, html)
  }

  return html
}

module.exports = fetch;
