const cheerio = require('cheerio');
const assert = require('assert');
const moment = require('moment-timezone');
const fetch = require('./caching-fetch');

const base = "http://mlb.mlb.com/news/probable_pitchers/?c_id=mlb&date="; // 2017/07/31

async function scrapeProbables(date) {
  const url = `${base}${moment(date).format("YYYY/MM/DD")}`
  console.log(`Fetching probables from: ${url}`)
  return await fetch(url).then(scrape)
}

function scrape(html) {
  const $ = cheerio.load(html);

  const result = [];

  $('.module').each((i, el) => {
    const teams = $(el).find('h4').text().split(' @ ');

    // .first includes game time information
    const first = $(el).find('.pitcher.first');

    const eastern_time = first.attr('eastern_time');
    const time = moment.tz(eastern_time, "America/New_York").toDate();
    const gid = first.attr('gid');

    const p1 = {
      name: $(first).find('h5 a').text(),
      h: $(first).find('h5 span').text(),
      pid: $(first).attr('pid') || null,
      tid: $(first).attr('tid'),
      team: teams[0],
      gid: $(first).attr('gid'),
      time,
      side: 'home'
    }

    const last = $(el).find('.pitcher.last');
    const p2 = {
      name: $(last).find('h5 a').text(),
      h: $(last).find('h5 span').text(),
      pid: $(last).attr('pid') || null,
      tid: $(last).attr('tid'),
      team: teams[1],
      gid,
      time,
      side: 'away'
    }

    result.push({ ...p1, vs: p2 })
    result.push({ ...p2, vs: p1 })
  });

  return result;
}

module.exports = scrapeProbables;
