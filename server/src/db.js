const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://probables_mongo_1:27017'

let db
let Starts

MongoClient.connect(url)
           .then(client => {
             db = client.db('mlbprobables')
             Starts = db.collection('starts'),
             Pitchers = db.collection('pitchers')
             Teams = db.collection('teams')
           })

const addStart = async(p) => {
  return await Starts.update({ gid: p.gid, tid: p.tid }, p, { upsert: true })
}

const addPitcher = async(p) => {
  return await Pitchers.update(
    { pid: p.pid },
    { pid: p.pid, name: p.name, team: p.team, tid: p.tid, nextStart: p },
    { upsert: true }
  )
}

const addTeam = async(p) => {
  console.log('addTeam', p.tid, p.team)
  return await Teams.update(
    { team: p.team },
    {
      $set: {
        team: p.team
      },
      $addToSet: {
        starts: p
      }
    },
    { upsert: true }
  )
}

const nextStart = async({ name }) => {
  return await Starts.findOne({ name: RegExp(name), time: { $gte: new Date() } })
}

const allStarts = async({ side }) => {
  return await Starts.find({ side, time: { $gte: new Date() } })
}

module.exports = {
  nextStart,
  addStart,
  allStarts,
  addPitcher,
  addTeam,
}
